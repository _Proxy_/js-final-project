
const dateInput = document.getElementById("date-input");
const calendarTable = document.querySelector(".calendar__tbody");

function getDaysInMonth(year, month) {
  return new Date(year, month + 1, 0).getDate();
}

function getFirstDayOfMonth(year, month) {
  return new Date(year, month, 1).getDay();
}

function createCalendar(date) {
  calendarTable.innerHTML = "";


  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();

 
  const daysInMonth = getDaysInMonth(year, month);
  const firstDayOfMonth = getFirstDayOfMonth(year, month);


  let currentDay = 1;


  for (let i = 0; i < 6; i++) {

    const tr = document.createElement("tr");
    tr.classList.add("calendar__tr");

и
    for (let j = 0; j < 7; j++) {
   
      const td = document.createElement("td");
      td.classList.add("calendar__td");


      if (i === 0 && j < firstDayOfMonth || currentDay > daysInMonth) {
        td.textContent = "";
      } else {
        td.textContent = currentDay;

        if (currentDay === day) {
          td.classList.add("calendar__td--selected");
        }
        currentDay++;
      }

      tr.appendChild(td);
    }

    calendarTable.appendChild(tr);
  }
}

dateInput.addEventListener("change", function () {
  const dateValue = dateInput.value;

  if (dateValue) {
    const date = new Date(dateValue);

    createCalendar(date);
  }
});

createCalendar(new Date());
