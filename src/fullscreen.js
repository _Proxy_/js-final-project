document.addEventListener('DOMContentLoaded', () => {
    const images = document.querySelectorAll('.gallery__img');

    images.forEach(image => {
      image.addEventListener('click', () => {
        const fullScreenDiv = document.createElement('div');
        fullScreenDiv.classList.add('fullscreen');

        const fullScreenImage = document.createElement('img');
        fullScreenImage.src = image.src;
        fullScreenImage.classList.add('fullscreen-img');

        fullScreenDiv.appendChild(fullScreenImage);
        document.body.appendChild(fullScreenDiv);

        fullScreenDiv.addEventListener('click', () => {
          document.body.removeChild(fullScreenDiv);
        });
      });
    });
  });