/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-unused-vars */
document.addEventListener('DOMContentLoaded', () => {
  const countries = {
    Россия: 10,
    Германия: 5,
    Италия: 2,
  };

  const countrySelect = document.getElementById('countrySelect');
  const calendar = document.getElementById('calendar');

  // Функция для заполнения стран в <select>
  function populateCountries() {
    for (const [country, available] of Object.entries(countries)) {
      const option = document.createElement('option');
      option.value = country;
      option.textContent = `${country} (${available} доступно)`;
      countrySelect.appendChild(option);
    }
  }

  // Функция для обработки регистрации
  document
    .getElementById('bookingForm')
    .addEventListener('submit', function (event) {
      event.preventDefault();
    });

  populateCountries();
});
